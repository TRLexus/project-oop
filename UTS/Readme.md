# Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)
<details><summary>Source Code</summary>

~~~java
import java.util.*;

class Akun {
    ArrayList<String> username = new ArrayList<String>();
    ArrayList<String> password = new ArrayList<String>();
    Scanner scan = new Scanner(System.in);
    void add() {
        System.out.print("Masukkan username = ");
        String user = scan.nextLine();
        System.out.print("Masukkan password = ");
        String pass = scan.nextLine();
        username.add(user);
        password.add(pass);
    }
    
    void login() {
        boolean repeat = true;
        do {
            System.out.print("Masukkan username = ");
            String user = scan.nextLine();
            System.out.print("Masukkan password = ");
            String pass = scan.nextLine();
            
            for (String element : username){
                if (element.contains(user)){
                    user = "true";
                }
            }
            
            for (String element : password){
                if (element.contains(pass)){
                    pass = "true";
                }
            }
            
            if (user != pass) {
                repeat = false;
            } else
                System.out.println("username atau password salah!!\n");
        } while (repeat == true); 
    }
}

class Buku {
    Scanner scan = new Scanner(System.in);
    void bookmark() {
        System.out.println("Buku telah dibookmark");
    }
    
    void like() {
        System.out.println();
    }
    
    void comment() {
        System.out.println("Comment");
        String line = scan.next();
        System.out.println("Komentar telah ditambahkan");
    }
    
    void add() {
        System.out.println("Buku telah ditambhkan");
    }
}

abstract class JenisBuku {
    Scanner scan = new Scanner(System.in);
    private String in;
    abstract Buku list();
}

class Novel extends JenisBuku {
    ArrayList<String> novel = new ArrayList<String>();
    void add() {
        System.out.print("Masukkan judul novel = ");
        in = scan.nextLine();
        novel.add(in);
        System.out.println("Novel " + in + " telah ditambahkan");
    }
    
    public Buku list() {
        System.out.println("List Novel");
        Buku buku = new Buku();
        return buku;
    }
}

class Comic extends JenisBuku {
    ArrayList<String> comic = new ArrayList<String>();
    void add() {
        System.out.print("Masukkan judul komik = ");
        in = scan.nextLine();
        comic.add(in);
        System.out.println("Komik " + in + " telah ditambahkan");
    }
    
    public Buku list() {
        System.out.println("List Comic");
        Buku buku = new Buku();
        return buku;
    }
}
class Bookmark extends JenisBuku {
    @Override
    public Buku list() {
        System.out.println("List Bookmark");
        Buku buku = new Buku();
        return buku;
    }
}

public class Main {
    public static void main(String[] algs) {
        Scanner scan = new Scanner(System.in);
        int input, repeat;
        
        do {
            repeat = 0;
            System.out.println("Selamat Datang di Webnovel");
            System.out.println("1. Daftar Novel\n2. Daftar Komik\n3. Bookmark\n4. Akun\n5. Keluar");
            System.out.print("Öption = ");
            input = scan.nextInt();
            switch (input) {
                case 1 :
                    JenisBuku Novel = new Novel();
                    Buku novel = Novel.list();
                    break;
                
                case 2 :
                    JenisBuku Comic = new Comic();
                    Buku comic = Comic.list();
                    break;
                
                case 3 :
                    JenisBuku Bookmark = new Bookmark();
                    Buku bookmark = Bookmark.list();
                    break;
                
                case 4 :
                    Akun akun = new Akun();
                    System.out.println("1. Login\n2. Registrasi");
                    System.out.print("Option = ");
                    input = scan.nextInt();
                    switch (input) {
                        case 1 :
                            akun.login();
                            break;
                        
                        case 2 :
                            akun.add();
                            break;
                        
                        default :
                            System.out.println("Input Salah!!");
                            break;
                    }
                    input = 4;
                    break;    
                
                case 5 :
                    repeat = 1;
                    System.out.println("Sampai Jumpa Lagi!!!");
                    break;
                
                default :
                    System.out.println("Input Salah!!");
                    break;
            }
            System.out.println("----------------------------");
        } while (repeat == 0);
    }
}
~~~

</details>


# Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)
<details><summary>Source Code</summary>

~~~java
import java.util.*;

class Akun {
    ArrayList<String> username = new ArrayList<String>();
    ArrayList<String> password = new ArrayList<String>();
    Scanner scan = new Scanner(System.in);
    void add() {
        System.out.print("Masukkan username = ");
        String user = scan.nextLine();
        System.out.print("Masukkan password = ");
        String pass = scan.nextLine();
        username.add(user);
        password.add(pass);
    }
    
    void login() {
        boolean repeat = true;
        do {
            System.out.print("Masukkan username = ");
            String user = scan.nextLine();
            System.out.print("Masukkan password = ");
            String pass = scan.nextLine();
            
            for (String element : username){
                if (element.contains(user)){
                    user = "true";
                }
            }
            
            for (String element : password){
                if (element.contains(pass)){
                    pass = "true";
                }
            }
            
            if (user != pass) {
                repeat = false;
            } else
                System.out.println("username atau password salah!!\n");
        } while (repeat == true); 
    }
}

class Buku {
    Scanner scan = new Scanner(System.in);
    void bookmark() {
        System.out.println("Buku telah dibookmark");
    }
    
    void like() {
        System.out.println();
    }
    
    void comment() {
        System.out.println("Comment");
        String line = scan.next();
        System.out.println("Komentar telah ditambahkan");
    }
    
    void add() {
        System.out.println("Buku telah ditambhkan");
    }
}

abstract class JenisBuku {
    Scanner scan = new Scanner(System.in);
    private String in;
    abstract Buku list();
}

class Novel extends JenisBuku {
    ArrayList<String> novel = new ArrayList<String>();
    void add() {
        System.out.print("Masukkan judul novel = ");
        in = scan.nextLine();
        novel.add(in);
        System.out.println("Novel " + in + " telah ditambahkan");
    }
    
    public Buku list() {
        System.out.println("List Novel");
        Buku buku = new Buku();
        return buku;
    }
}

class Comic extends JenisBuku {
    ArrayList<String> comic = new ArrayList<String>();
    void add() {
        System.out.print("Masukkan judul komik = ");
        in = scan.nextLine();
        comic.add(in);
        System.out.println("Komik " + in + " telah ditambahkan");
    }
    
    public Buku list() {
        System.out.println("List Comic");
        Buku buku = new Buku();
        return buku;
    }
}
class Bookmark extends JenisBuku {
    @Override
    public Buku list() {
        System.out.println("List Bookmark");
        Buku buku = new Buku();
        return buku;
    }
}

public class Main {
    public static void main(String[] algs) {
        Scanner scan = new Scanner(System.in);
        int input, repeat;
        
        do {
            repeat = 0;
            System.out.println("Selamat Datang di Webnovel");
            System.out.println("1. Daftar Novel\n2. Daftar Komik\n3. Bookmark\n4. Akun\n5. Keluar");
            System.out.print("Öption = ");
            input = scan.nextInt();
            switch (input) {
                case 1 :
                    JenisBuku Novel = new Novel();
                    Buku novel = Novel.list();
                    break;
                
                case 2 :
                    JenisBuku Comic = new Comic();
                    Buku comic = Comic.list();
                    break;
                
                case 3 :
                    JenisBuku Bookmark = new Bookmark();
                    Buku bookmark = Bookmark.list();
                    break;
                
                case 4 :
                    Akun akun = new Akun();
                    System.out.println("1. Login\n2. Registrasi");
                    System.out.print("Option = ");
                    input = scan.nextInt();
                    switch (input) {
                        case 1 :
                            akun.login();
                            break;
                        
                        case 2 :
                            akun.add();
                            break;
                        
                        default :
                            System.out.println("Input Salah!!");
                            break;
                    }
                    input = 4;
                    break;    
                
                case 5 :
                    repeat = 1;
                    System.out.println("Sampai Jumpa Lagi!!!");
                    break;
                
                default :
                    System.out.println("Input Salah!!");
                    break;
            }
            System.out.println("----------------------------");
        } while (repeat == 0);
    }
}
~~~

</details>


# Mampu menjelaskan konsep dasar OOP
Konsep dasar pemrograman berorientasi objek (OOP) adalah paradigma pemrograman yang berfokus pada objek sebagai unit dasar dalam pembangunan aplikasi. OOP melibatkan pengelompokan objek-objek terkait ke dalam kelas serta memiliki atribut dan metode yang digunakan untuk memanipulasi objek-objek tersebut. Berikut adalah 4 pilar besar dalam paradigma OOP, yaitu: 
- Encapsulation: Encapsulation (Enkapsulasi) mengacu pada pembungkusan data dan metode yang terkait dalam satu unit yang disebut kelas. Tujuannya adalah untuk mencegah akses langsung ke data dan metode dari luar kelas, dan memastikan bahwa data hanya dapat diakses dan dimanipulasi melalui metode yang ditentukan dalam kelas. 
- Inheritance: Inheritance (Warisan) mengizinkan pembuatan kelas baru dari kelas yang sudah ada dengan cara mewarisi atribut dan metode dari kelas yang sudah ada. Kelas baru ini disebut kelas turunan, dan kelas yang diwarisi disebut kelas induk atau superclass. Tujuan dari inheritance adalah untuk mengurangi duplikasi kode dan mempermudah pemeliharaan kode. 
- Polymorphism: Polymorphism (Polimorfisme) mengacu pada kemampuan objek untuk merespons secara berbeda terhadap metode yang sama. Hal ini terjadi karena objek dapat memiliki tipe yang berbeda, namun dapat dipanggil melalui metode yang sama. Polymorphism dapat dicapai melalui penggunaan overriding dan overloading. 
- Abstraction: Abstraction (Abstraksi) mengacu pada penyembunyian detail implementasi objek dan hanya menampilkan fitur-fitur penting untuk pengguna. Hal ini dapat dicapai melalui penggunaan kelas abstrak dan antarmuka.

# Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)
![Lampiran](Soal_Nomor_4.gif)

# Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)
![Lampiran](Soal_Nomor_5.gif)

# Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)
![Lampiran](Soal_Nomor_6.gif)

# Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?
Webnovel merupakan aplikasi yang menyediakan layanan membaca novel atau comic kepada pengguna dan juga publik. Aplikasi ini sangat berpaku kepada apa yang bisa mereka berikan, seberapa banyak yang bisa mereka berikan, dan juga seberapa banyak yang bisa mereka dapatkan.
Aplikasi ini memiliki berbagai macam kebutuhan beserta dengan fungsinya, terutama ketika hal tersebut berkaitan dengan fungsi utamanya yaitu penyediaan untuk pembacaan novel atau comic. Aplikasi ini juga memiliki fitur seperti novel yang berbayar dan juga banner serta title yang dapat dikoleksi oleh para pengguna aplikasi yang sudah melakukan registrasi.


# Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)
<details><summary>Use Case Table</summary>

| Nomor | User | Bisa | Nilai Prioritas |
| ------ | ------ | ------ | ------ |
| 1 | Public | Membaca Novel | 100 |
| 2 | Public | Membaca Komik | 100 |
| 3 | User | Menambahkan Komentar Pada Chapter | 80 |
| 4 | User | Menambahkan Komentar Pada Baris | 80 |
| 5 | User | Menambahkan Komentar Pada Halaman | 80 |
| 6 | User | Memberikan Like | 80 |
| 7 | User | Memperbarui Status Like | 80 |
| 8 | User | Memberikan Rating | 80 |
| 9 | User | Menambahkan Komentar Pada Rating | 80 |
| 10 | User | Menambahkan Bookmark | 70 |
| 11 | Public | Membuat Akun | 80 |
| 12 | User | Login | 80 |
| 13 | User | Menambahkan Novel | 80 |
| 14 | User | Menambahkan Komik | 80 |
| 15 | User | Mengajukan Penghapusan Akun | 60 |
| 16 | User | Mengajukan Penghapusan Novel | 60 |
| 17 | User | Mengajukan Penghapusan Komik | 60 |
| 18 | Admin | Menghapus Akun | 60 |
| 19 | Admin | Menghapus Novel | 60 |
| 20 | Admin | Menghapus Komik | 60 |
| 21 | User | Menghubungi Costumer Service | 50 |
| 22 | User | Melaporkan Masalah | 50 |
| 23 | Admin | Melihat Laporan Masalah | 50 |
| 24 | User | Mengubah Password | 40 |
| 25 | User | Mengganti Foto Profile | 30 |
| 26 | User | Mengubah Nama Panggilan | 30 |
| 27 | Admin | Menyimpan Data | 90 |
| 28 | Admin | Mengkategorikan Buku | 60 |
| 29 | User | Membeli Koin | 60 |
| 30 | User | Membeli Chapter dengan Koin | 60 |
| 31 | User | Mendapatkan Koin dari Login Harian | 60 |
| 32 | User | Mendapatkan Koin dari Event | 60 |
| 33 | User | Membayar Pembelian dengan Transfer Bank | 60 |
| 34 | User | Membayar Pembelian dengan E-Money | 60 |
| 35 | Admin | Membuat Title | 20 |
| 36 | Admin | Membuat Banner | 20 |
| 37 | Admin | Memberikan Title | 20 |
| 38 | Admin | Memberikan Banner | 20 |
| 39 | Admin | Menetapkan Masa Berlaku Banner | 20 |
| 40 | Admin | Menghapus Banner | 20 |
| 41 | Admin | Menetapkan Masa Berlaku Title | 20 |
| 42 | Admin | Menghapus Title | 20 |
| 43 | User | Mendapatkan Title | 20 |
| 44 | User | Mendapatkan Banner | 20 |
| 45 | User | Membeli Title dengan Koin | 20 |
| 46 | User | Membeli Banner dengan Koin | 20 |
| 47 | User | Menambahkan Cover untuk Novel | 70 |
| 48 | User | Menambahkan Cover untuk Comic | 70 |
| 49 | User | Menghapus Komentar pada Novel yang Dibuat | 30 |
| 50 | User | Menghapus Komentar pada Comic yang Dibuat | 30 |
| 51 | User | Menghapus Komentar Milik Pribadi | 40 |
| 52 | User | Menambahkan Sinopsis pada Novel yang Dibuat | 80 |
| 53 | User | Menambahkan Sinopsis pada Comic yang Dibuat | 80 |
| 54 | User | Menghapus Sinopsis pada Novel yang Dibuat | 80 |
| 55 | User | Menghapus Sinopsis pada Comic yang Dibuat | 80 |
| 56 | Admin | Menghapus Sinopsis untuk Novel | 80 |
| 57 | Admin | Menghapus Sinopsis untuk Comic | 80 |
| 58 | Admin | Menghapus Komentar dalam Novel | 70 |
| 59 | Admin | Menghapus Komentar dalam Comic | 70 |
| 60 | User | Membalas Komentar | 60 |
| 61 | User | Menggunakan File GIF pada Komentar | 50 |
| 62 | Admin | Menyediakan File GIF | 50 |
| 63 | Admin | Menghapus File GIF | 50 |
| 64 | User | Membalas Komentar dengan Emote | 50 |
| 65 | User | Memberikan Like pada Komentar | 50 |
| 66 | User | Melaporkan Komentar | 60 |
| 67 | User | Melaporkan Novel | 60 |
| 68 | User | Melaporkan Comic | 60 |
| 69 | Admin | Menyimpan Laporan dari User | 60 |
| 70 | Admin | Menanggapi Laporan dari User | 60 |

</details>

![Class Diagram](UTS/class_diagram.png)

# Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)


# Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)
Saya mencoba menggunakan netbeans sebagai basis data dari aplikasi webnovel karena pada netbeans sudah terdapat jdk sehingga memudahkan dalam pembuatan aplikasi java. Netbeans juga sangat mudah untuk digunakan dan memiliki berbagai macam fungsi yang dapat membantu dalam proses pembuatan aplikasi. Selain itu, saya menggunakan derby untuk membantu membuat koneksi antara program java dengan database yang sudah dibuat pada netbeans.
![Lampiran](Soal_Nomor_10.gif)
Sejauh ini saya belum bisa membuat koneksi yang tepat antara program java dengan basis datanya.

