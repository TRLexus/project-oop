# Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital: 
**Use case user**
| Nomor | Bisa | Prioritas |
| ------ | ------ | ------ |
| 1 | Menambahkan Komentar Pada Chapter | 80 |
| 2 | Menambahkan Komentar Pada Baris | 80 |
| 3 | Menambahkan Komentar Pada Halaman | 80 |
| 4 | Memberikan Like | 80 |
| 5 | Memperbarui Status Like | 80 |
| 6 | Memberikan Rating | 80 |
| 7 | Menambahkan Komentar Pada Rating | 80 |
| 8 | Menambahkan Bookmark | 70 |
| 9 | Login | 80 |
| 10 | Menambahkan Novel | 80 |
| 11 | Menambahkan Komik | 80 |
| 12 | Mengajukan Penghapusan Akun | 60 |
| 13 | Mengajukan Penghapusan Novel | 60 |
| 14 | Mengajukan Penghapusan Komik | 60 |
| 15 | Menghubungi Costumer Service | 50 |
| 16 | Melaporkan Masalah | 50 |
| 17 | Mengubah Password | 40 |
| 18 | Mengganti Foto Profile | 30 |
| 19 | Mengubah Nama Panggilan | 30 |
| 20 | Membeli Koin | 60 |
| 21 | Membeli Chapter dengan Koin | 60 |
| 22 | Mendapatkan Koin dari Login Harian | 60 |
| 23 | Mendapatkan Koin dari Event | 60 |
| 24 | Membayar Pembelian dengan Transfer Bank | 60 |
| 25 | Membayar Pembelian dengan E-Money | 60 |
| 26 | Mendapatkan Title | 20 |
| 27 | Mendapatkan Banner | 20 |
| 28 | Membeli Title dengan Koin | 20 |
| 29 | Membeli Banner dengan Koin | 20 |
| 30 | Menambahkan Cover untuk Novel | 70 |
| 31 | Menambahkan Cover untuk Comic | 70 |
| 32 | Menghapus Komentar pada Novel yang Dibuat | 30 |
| 33 | Menghapus Komentar pada Comic yang Dibuat | 30 |
| 34 | Menghapus Komentar Milik Pribadi | 40 |
| 35 | Menambahkan Sinopsis pada Novel yang Dibuat | 80 |
| 36 | Menambahkan Sinopsis pada Comic yang Dibuat | 80 |
| 37 | Menghapus Sinopsis pada Novel yang Dibuat | 80 |
| 38 | Menghapus Sinopsis pada Comic yang Dibuat | 80 |
| 39 | Membalas Komentar | 60 |
| 40 | Menggunakan File GIF pada Komentar | 50 |
| 41 | Membalas Komentar dengan Emote | 50 |
| 42 | Memberikan Like pada Komentar | 50 |
| 43 | Melaporkan Komentar | 60 |
| 44 | Melaporkan Novel | 60 |
| 45 | Melaporkan Comic | 60 |

**Use case manajemen perusahaan**
| Nomor | Bisa | Prioritas |
| ------ | ------ | ------ |
| 1 | Menghapus Akun | 60 |
| 2 | Menghapus Novel | 60 |
| 3 | Menghapus Komik | 60 |
| 4 | Melihat Laporan Masalah | 50 |
| 5 | Menyimpan Data | 90 |
| 6 | Mengkategorikan Buku | 60 |
| 7 | Membuat Title | 20 |
| 8 | Membuat Banner | 20 |
| 9 | Memberikan Title | 20 |
| 10 | Memberikan Banner | 20 |
| 11 | Menetapkan Masa Berlaku Banner | 20 |
| 12 | Menghapus Banner | 20 |
| 13 | Menetapkan Masa Berlaku Title | 20 |
| 14 | Menghapus Title | 20 |
| 15 | Menghapus Sinopsis untuk Novel | 80 |
| 16 | Menghapus Sinopsis untuk Comic | 80 |
| 17 | Menghapus Komentar dalam Novel | 70 |
| 18 | Menghapus Komentar dalam Comic | 70 |
| 19 | Menyediakan File GIF | 50 |
| 20 | Menghapus File GIF | 50 |
| 21 | Menyimpan Laporan dari User | 60 |
| 22 | Menanggapi Laporan dari User | 60 |

**Use case direksi perusahaan (dashboard, monitoring, analisis)**
| Nomor | Bisa | Prioritas |
| ------ | ------ | ------ |
| 1 | Melihat Pertumbuhan Pengguna (perbulan) | 100 |
| 2 | Mengelompokkan Pengguna Berdasarkan Negara | 80 |
| 3 | Mengelompokkan Pengguna Berdasarkan Jenis Akun | 80 |
| 4 | Menganalisis Statistik Pembelian Koin | 100 |
| 5 | Menganalisis Statistik Views | 100 |
| 6 | Mengelompokkan Novel dan Komik Berdasarkan Jumlah Likes dan Bookmark | 70 |

# Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital
![Class Diagram](Dokumentasi/class_diagram.png)

# Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
Single Responsibily Principle (SRP)
~~~python
from flask import Flask, render_template, request, session, redirect, url_for
import mysql.connector

app = Flask(__name__)
app.secret_key = "key"  # Replace with a strong secret key

db = mysql.connector.connect(
    host="localhost",
    user="name",  # Replace with your MySQL username
    password="pass",  # Replace with your MySQL password
    database="webnovel"
)
~~~
Kode diatas termasuk kedalam kelas aplikasi yang bertujuan untuk mengelola koneksi dan operasi pada database. Kelas ini hanya memiliki satu tujuan, yaitu mengatur koneksi dan operasi yang dilakukan program agar bisa terhubung dengan database

# Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih


# Mampu menunjukkan dan menjelaskan konektivitas ke database
~~~python
@app.route("/")
def index():
    cursor = db.cursor()
    cursor.execute("SELECT * FROM novel")
    novels = cursor.fetchall()
    cursor.execute("SELECT * FROM comic")
    comics = cursor.fetchall()

    if 'username' in session:
        username = session['username']
        cursor.execute("SELECT * FROM bookmarks WHERE username = %s", (username,))
        bookmarks = cursor.fetchall()
        return render_template("index.html", username=username, novels=novels, comics=comics, bookmarks=bookmarks)
    
    return render_template("index.html", novels=novels, comics=comics)
...
~~~
Pertama index di definisikan, ini adalah kelas yang bertanggung jawab menjaga koneksi ke database. Di dalam kelas ini, variabe seperti novels, comics, dan users di inisialisasi.
Kemudian kelas ini akan mengembalikan isi dari data dalam tabel dengan nama yang sama lalu memasukannya kedalam variabel-variabel yang bersangkutan.
Cara untuk memanggil nilai dari variabel-variabel tersebut adalah sebagai berikut :
~~~HTML
...
<div class="novels">
            <h2>Novel</h2>
            {% if novels %}
                {% for novel in novels %}
                    <h3>{{ novel[1] }}</h3>
                    <p>{{ novel[2] }}</p>
                {% endfor %}
            {% else %}
                <p>Tidak ada novel yang tersedia.</p>
            {% endif %}
        </div>
...
~~~

# Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya
Web Service yang dipakai : Python Flask
![Web Service](Dokumentasi/Web_Service.PNG)

# Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital
Halaman Utama
![Index](Dokumentasi/Index.PNG)
Halaman Login
![Login](Dokumentasi/Login.PNG)
Halaman Register
![Register](Dokumentasi/Register.PNG)

# Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital
~~~HTML
<body>
    <div class="container">
        <h1>WebNovel</h1>
        
        <div class="welcome">
            {% if username %}
                <h2>Selamat datang, {{ username }}!</h2>
            {% else %}
                <p>Selamat datang di WebNovel. Silahkan <a href="/login">login</a> atau <a href="/register">register</a> untuk melanjutkan.</p>
            {% endif %}
        </div>

        ........
~~~

# Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
Link Video :

# Bonus: Mendemonstrasikan penggunaan Machine Learning
Link Video :
